# DoIT B1G UI <br />
## Quick start

1.  Make sure that you have Node v8 or above installed.
2.  Clone this repo using `git clone git@gitlab.com:cs506-doit-sts/doitbig-ui.git`
3.  Move to the appropriate directory: `cd doitbig-ui`.<br />
4.  Run `npm install` in order to install dependencies and clean the git repo.<br />
5.  At this point you can run `npm start` to see the app at `http://localhost:3000`.

## Routes

Currently we have the following pages active:
- http://localhost:3000/profile
- http://localhost:3000/signin
- http://localhost:3000/signup

## Testing

To run the automated Jest tests simply run: 

    $ npm test

## License

This project is licensed under the MIT license, Copyright (c) 2018 DoIT Big. For more information see `LICENSE.md`.